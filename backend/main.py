# coding=utf-8
import falcon.asgi
from waitress import serve

from resources import ipmi_server, ssh_server, action, task

app = falcon.App(cors_enable=True)

# IPMI routers
ipmi_server_resource = ipmi_server.IPMIServerResource()
app.add_route("/api/ipmi-servers", ipmi_server_resource)
app.add_route("/api/ipmi-servers/{resource_id:int}", ipmi_server_resource)
app.add_route("/api/ipmi-servers/{resource_id:int}/power-shutdown", ipmi_server_resource, suffix="shutdown")
app.add_route("/api/ipmi-servers/{resource_id:int}/power-on", ipmi_server_resource, suffix="start")
app.add_route("/api/ipmi-servers/{resource_id:int}/refresh", ipmi_server_resource, suffix="refresh")

# SSH routers
ssh_server_resource = ssh_server.SSHServerResource()
app.add_route("/api/ssh-servers", ssh_server_resource)
app.add_route("/api/ssh-servers/{resource_id:int}", ssh_server_resource)
app.add_route("/api/ssh-servers/{resource_id:int}/shutdown", ssh_server_resource, suffix="shutdown")
app.add_route("/api/ssh-servers/{resource_id:int}/restart", ssh_server_resource, suffix="restart")
app.add_route("/api/ssh-servers/{resource_id:int}/collect", ssh_server_resource, suffix="collect")

# Action routers
action_resource = action.ActionResource()
app.add_route("/api/actions", action_resource)
app.add_route("/api/actions/{resource_id:int}", action_resource)

# Task routers
task_resource = task.TaskResource()
app.add_route("/api/tasks", task_resource)
app.add_route("/api/tasks/{resource_id:int}", task_resource)
app.add_route("/api/tasks/{resource_id:int}/exec-records", task_resource, suffix="exec_record")

if __name__ == '__main__':
    serve(app, host="0.0.0.0", port=8189)

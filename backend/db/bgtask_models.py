# coding=utf-8
import enum

from peewee import *

from .base import BaseModel


class StateEnum(enum.StrEnum):
    """
    Wait: the task has not been put into the task queue.
    Ready: task has been put into the task queue, but not executed
    Running: task is running
    Finish: task done
    """
    WAIT = "Wait"
    READY = "Ready"
    RUNNING = "Running"
    FINISH = "Finish"


class ResultEnum(enum.StrEnum):
    SUCCESS = "Success"
    FAIL = "Fail"


class BgTask(BaseModel):
    task_id = CharField(primary_key=True, max_length=32)

    fn = CharField()
    args = BitField(null=True)
    kwargs = BitField(null=True)

    err_msg = CharField(max_length=1024, null=True)

    state = CharField(choices=[(member.value, member.name) for member in StateEnum])
    result = CharField(choices=[(member.value, member.name) for member in ResultEnum], null=True)
    return_value = BitField(null=True)

    start_time = DateTimeField(null=True, formats=['%Y-%m-%d %H:%M:%S'])
    finish_time = DateTimeField(null=True, formats=['%Y-%m-%d %H:%M:%S'])

    created_time = DateTimeField(formats=['%Y-%m-%d %H:%M:%S'])

    class Meta:
        db_table = "django_bgtask"

# coding=utf-8
from peewee import SqliteDatabase, MySQLDatabase, Model, CharField, DateTimeField

import settings

db = SqliteDatabase(settings.SQLITE3_DB)


class BaseModel(Model):

    class Meta:
        database = db

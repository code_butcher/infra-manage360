# coding=utf-8
import enum
from datetime import datetime

from peewee import *

from .base import BaseModel


class ActionCategory(enum.StrEnum):
    SHELL = "shell"
    PYTHON = "python"


class ExecStatus(enum.StrEnum):
    CREATED = "created"
    RUNNING = "running"
    SUCCESS = "success"
    FAIL = "fail"


class IPMIServer(BaseModel):
    ipaddress = IPField(unique=True)
    user = CharField(null=False)
    password = CharField(null=False)
    location = CharField()
    owner = CharField()
    desc = CharField(default="")
    created_time = DateTimeField(default=datetime.now, formats=['%Y-%m-%d %H:%M:%S'])
    updated_time = DateTimeField(default=datetime.now, formats=['%Y-%m-%d %H:%M:%S'])


class SSHServer(BaseModel):
    ipaddress = IPField(unique=True)
    user = CharField(null=False)
    password = CharField(null=False)
    port = IntegerField(default=22)
    desc = CharField(default="")
    created_time = DateTimeField(default=datetime.now, formats=['%Y-%m-%d %H:%M:%S'])
    updated_time = DateTimeField(default=datetime.now, formats=['%Y-%m-%d %H:%M:%S'])


class OSBaseInfo(BaseModel):
    """ 操作系统基础信息 """
    ssh_server_id = IntegerField(unique=True)

    hostname = CharField()
    os_release = CharField()
    serial_number = CharField()
    system_uuid = CharField()
    uptime = CharField()
    memory = CharField(verbose_name="json 字符串")
    nics = CharField(verbose_name="json 字符串")
    filesystem = TextField(verbose_name="json 字符串")
    cpu = TextField(verbose_name="json 字符串")
    collect_time = DateTimeField(default=datetime.now, formats=['%Y-%m-%d %H:%M:%S'])


class Action(BaseModel):
    name = CharField(unique=True)
    category = CharField(choices=[(member.value, member.name) for member in ActionCategory])
    exec = CharField(verbose_name="执行程序，shell 默认 /usr/bin/sh, python 默认 /usr/bin/python3")
    content = TextField()
    timeout = IntegerField(default=30, verbose_name="运行超时时间，单位秒")
    retry = IntegerField(default=0, verbose_name="失败后重试次数， 默认不重试")
    desc = CharField(default="")

    created_time = DateTimeField(default=datetime.now, formats=['%Y-%m-%d %H:%M:%S'])


class Task(BaseModel):
    name = CharField()
    desc = CharField(default="")

    created_time = DateTimeField(default=datetime.now, formats=['%Y-%m-%d %H:%M:%S'])


class TaskStep(BaseModel):
    task_id = IntegerField()
    action_id = IntegerField()
    ssh_server_id = IntegerField()
    order = IntegerField()


class TaskExecRecord(BaseModel):
    task_id = IntegerField()
    status = CharField(choices=[(member.value, member.name) for member in ExecStatus])
    start_time = DateTimeField(default=datetime.now, formats=['%Y-%m-%d %H:%M:%S'])
    end_time = DateTimeField(null=True, formats=['%Y-%m-%d %H:%M:%S'])

    created_time = DateTimeField(default=datetime.now, formats=['%Y-%m-%d %H:%M:%S'])
    updated_time = DateTimeField(default=datetime.now, formats=['%Y-%m-%d %H:%M:%S'])


class StepExecRecord(BaseModel):
    task_id = IntegerField()
    task_exec_record_id = IntegerField()
    task_step_id = IntegerField()

    status = CharField(choices=[(member.value, member.name) for member in ExecStatus])
    error_message = TextField(default="执行成功")
    return_code = IntegerField(default=0)
    return_text = TextField(default="")

    start_time = DateTimeField(default=datetime.now, formats=['%Y-%m-%d %H:%M:%S'])
    end_time = DateTimeField(default=datetime.now, formats=['%Y-%m-%d %H:%M:%S'])

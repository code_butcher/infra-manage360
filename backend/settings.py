# coding=utf-8
import logging
import pathlib

DEBUG = True
PROJECT_BASE_PATH = pathlib.Path(__file__).absolute().parent
SQLITE3_DB = PROJECT_BASE_PATH.joinpath("app.db")

BGTASK_SERVER = ("127.0.0.1", 23323)
BGTASK_WORK_THREADS = 10


def init_log(level):
    logger = logging.getLogger("bgtask")
    logger.setLevel(level)
    logger.propagate = True

    h = logging.StreamHandler()
    h.setLevel(level)
    logger.addHandler(h)

    logger = logging.getLogger("web")
    logger.setLevel(level)
    logger.propagate = True

    h = logging.StreamHandler()
    h.setLevel(level)
    logger.addHandler(h)

init_log(logging.DEBUG)

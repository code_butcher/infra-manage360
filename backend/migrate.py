# coding=utf-8
from datetime import datetime
from playhouse.migrate import *

from db import models, bgtask_models
from db.base import db

db.connect()

db.create_tables([
    models.IPMIServer, models.SSHServer, models.OSBaseInfo,
    models.Action, models.Task, models.TaskStep, models.TaskExecRecord, models.StepExecRecord,
    bgtask_models.BgTask,
])

# migrator = SqliteMigrator(db)
# migrate(
#     # migrator.add_column('ipmiserver', 'location', CharField(default="")),
#     migrator.add_column('osbaseinfo', 'collect_time', DateTimeField(default=datetime.now))
# )

mkdir -p /home/leon/infra-manage-360/logs;
cd /home/leon/infra-manage-360/backend;
ps -ef |grep "python3 main.py" |grep -v grep| awk '{print $2}' | xargs kill
sleep 1;
nohup python3 main.py > /home/leon/infra-manage-360/logs/app.log &2>1 &

#/home/leon/nginx/sbin/nginx -c /home/leon/infra-manage-360/nginx.conf
/home/leon/nginx/sbin/nginx -s reload -c /home/leon/infra-manage-360/nginx.conf

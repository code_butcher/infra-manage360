import { createRouter, createWebHistory } from 'vue-router'
import index from '../views/index.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      component: index,
      redirect: "/machine/ipmi",
      children: [
        {
          path: "/machine/ipmi",
          name: "ipmi",
          meta: {
            title: "IPMI"
          },
          component: () => import('../views/ipmi/index.vue')
        },
        {
          path: "/machine/ssh",
          name: "ssh",
          meta: {
            title: "SSH"
          },
          component: () => import('../views/ssh/index.vue')
        },
        {
          path: "/machine/ssh/detail",
          name: "ssh-detail",
          component: () => import('../views/ssh/detail.vue')
        },
      ]
    },
    {
      path: '/login',
      name: 'login',
      meta: {
        title: "登录"
      },
      component: () => import('../views/login.vue')
    },
    {
      path: '/register',
      name: 'register',
      meta: {
        title: "注册"
      },
      component: () => import('../views/register.vue')
    }
  ]
})

// 设置页面 title
router.beforeEach((to, from, next) => {
  let title = to.meta.title;
  if (title === undefined){
    title = "InfraManage360"
  }
  window.document.title = title;
  next()
})

export default router

export function getCookie(name) {
    let reg = RegExp(name + '=([^;]+)')
    console.log(reg)
    let arr = document.cookie.match(reg)
    if (arr) {
        return arr[1]
    } else {
        return ''
    }
}

const DEBUG = true;
let BASE_URL = '';

if(DEBUG){
    BASE_URL = "http://localhost:8189/api"
    // BASE_URL = "http://10.10.236.52:8188/api"
}else{
    BASE_URL = "http://" + window.location.host + "/api"
}

export {
    DEBUG,
    BASE_URL
}
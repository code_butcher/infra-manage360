import http from '@/utils/http'

export function addIpmi(ipaddress, user, password, location, owner, desc) {
    return http.axiosInstance({
        method: "POST",
        url: "/ipmi-servers",
        data: {
            ipaddress: ipaddress,
            user: user,
            location, owner,
            password: password,
            desc: desc
        }
    })
}

export function deleteIpmi(r_id) {
    return http.axiosInstance({
        method: "DELETE",
        url: "/ipmi-servers/" + r_id,
    })
}

export function updateIpmi(r_id, user, password, location, owner, desc) {
    return http.axiosInstance({
        method: "PATCH",
        url: "/ipmi-servers/" + r_id,
        data: {
            user: user,
            location, owner,
            password: password,
            desc: desc
        }
    })
}

export function ipmiList(page=1, page_size=10) {
    return http.axiosInstance({
        method: "GET",
        url: "/ipmi-servers",
        params: {
            page, page_size
        }
    })
}

export function getIpmi(r_id) {
    return http.axiosInstance({
        method: "GET",
        url: "/ipmi-servers/" + r_id,
    })
}

export function getStatus(r_id) {
    return http.axiosInstance({
        method: "GET",
        url: "/ipmi-servers/" + r_id + "/refresh",
    })
}

export function startMachine(r_id) {
    return http.axiosInstance({
        method: "GET",
        url: "/ipmi-servers/" + r_id + "/power-on",
    })
}

export function shutdownMachine(r_id) {
    return http.axiosInstance({
        method: "GET",
        url: "/ipmi-servers/" + r_id + "/power-shutdown",
    })
}
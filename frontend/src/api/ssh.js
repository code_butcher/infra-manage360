import http from "@/utils/http";

export function addSsh(ipaddress, user, password, port, desc) {
  return http.axiosInstance({
    method: "POST",
    url: "/ssh-servers",
    data: {
      ipaddress, user,  port, password, desc,
    },
  });
}

export function deleteSsh(r_id) {
  return http.axiosInstance({
    method: "DELETE",
    url: "/ssh-servers/" + r_id,
  });
}

export function updateSsh(r_id, user, password, port, desc) {
  return http.axiosInstance({
    method: "PATCH",
    url: "/ssh-servers/" + r_id,
    data: {
      user: user,
      port,
      password: password,
      desc: desc,
    },
  });
}

export function sshList(page = 1, page_size = 10) {
  return http.axiosInstance({
    method: "GET",
    url: "/ssh-servers",
    params: {
      page,
      page_size,
    },
  });
}

export function getSsh(r_id) {
    return http.axiosInstance({
      method: "GET",
      url: "/ssh-servers/" + r_id,
    });
  }

export function shutdownOs(r_id) {
  return http.axiosInstance({
    method: "GET",
    url: "/ssh-servers/" + r_id + "/shutdown",
  });
}

export function restartOs(r_id) {
  return http.axiosInstance({
    method: "GET",
    url: "/ssh-servers/" + r_id + "/restart",
  });
}

export function collectOs(r_id) {
  return http.axiosInstance({
    method: "GET",
    url: "/ssh-servers/" + r_id + "/collect",
  });
}

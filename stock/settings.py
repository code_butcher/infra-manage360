# coding=utf-8
SEND_REAL_PRICE = True
COLLECT_INTERVAL = 30

DEFAULT_AMPLITUDE_ALERT_THRESHOLD = 2
DEFAULT_AMPLITUDE_ALERT_INTERVAL = 60 * 10

SECRET = "SECe762f0627e189412e4e9add0615d4f67f4e099531f4f4df76fe771e4e2087c25"
ACCESS_TOKEN = "0961e802304a74b2bc003d41b8af813a49f68528f9c01325be92134cc42b2d84"

stock_conf = {
    "sz002156": {
        "amplitude_alert_threshold": 1.5,
        "amplitude_alert_interval": 600
    },
}


class StockConf(dict):
    def amplitude_alert_threshold(self, code):
        return self.get(code, {}).get("amplitude_alert_threshold", DEFAULT_AMPLITUDE_ALERT_THRESHOLD)

    def amplitude_alert_interval(self, code):
        return self.get(code, {}).get("amplitude_alert_interval", DEFAULT_AMPLITUDE_ALERT_INTERVAL)


STOCKS = StockConf(stock_conf)
